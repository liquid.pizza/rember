package com.example.rember

import android.support.design.widget.NavigationView
import android.util.Log
import android.view.Menu
import android.view.Menu.NONE
import com.beust.klaxon.Klaxon

class DataHandler {
    var myData: ArrayList<Item> = arrayListOf()

    var filteredData: ArrayList<Item> = arrayListOf()
    var cntFilter: ArrayList<String> = arrayListOf()
    var tags: MutableMap<String, Int> = mutableMapOf()

    lateinit var tagMenu: Menu

    fun filterDataByTags(queryTags: ArrayList<String>): Boolean
    {
        cntFilter = queryTags

        for (qTag in queryTags)
        {
            filteredData = myData.filter { item -> item.tag.contains(qTag, true) } as ArrayList<Item>
        }

        return true
    }

    fun filterDataByTag(queryTag: String): Boolean
    {
        cntFilter = arrayListOf(queryTag)

        filteredData = myData.filter { item -> item.tag.contains(queryTag, true) } as ArrayList<Item>

        return true
    }

    fun updateTags()
    {
        tags.clear()

        for (item in myData)
        {
            tags[item.tag] = tags.getOrDefault(item.tag, 0) + 1
        }
        Log.e(LOG_TAG, dataHandler.tags.toString())
        updateNavView()
    }

    fun updateNavView()
    {
        tagMenu.clear()

        for (tag in tags)
        {
            // add a new entry to the menu group
            // the new item has to be checkable
            tagMenu.add(R.id.nav_menu_group, NONE, myData.size - tag.value, tag.key + " (" + tag.value + ")").isCheckable = true
        }

    }

    fun removeData(data: Item)
    {
        // TODO handle a filtered list
        myData.remove(data)
        filterDataByTags(cntFilter)
        recyclerView.swapAdapter(MyAdapter(filteredData), true)
        configFile.writeText(Klaxon().toJsonString(myData))

        updateTags()
    }

    fun addData(data: Item)
    {
        // TODO handle a filtered list
        myData.add(data)
        filterDataByTags(cntFilter)
        recyclerView.swapAdapter(MyAdapter(filteredData), true)
        configFile.writeText(Klaxon().toJsonString(myData))

        updateTags()
    }
}