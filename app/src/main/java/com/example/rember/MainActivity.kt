package com.example.rember

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.support.design.internal.NavigationMenu
import android.support.design.widget.NavigationView
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.Menu.NONE
import android.widget.SearchView
import android.widget.Toast
import com.beust.klaxon.Klaxon

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_holder_layout.view.*
import java.io.File
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

lateinit var configFile: File

var LOG_TAG: String = "Rember"
var MIN_DISTANCE: Float = 20F

var dataHandler: DataHandler = DataHandler()
lateinit var viewAdapter: MyAdapter
lateinit var recyclerView: RecyclerView

class MainActivity : AppCompatActivity() {

    private lateinit var viewManager: RecyclerView.LayoutManager

    var mCounter: Int = 0

    private lateinit var dirPath: File

    private lateinit var oldFilterItem: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        // add a submenu for the tags
        dataHandler.tagMenu = nav_view.menu.addSubMenu(R.id.nav_menu_group, NONE, NONE, R.string.tags)

//        toolbar.setOnClickListener { toggleSearchBar() }
        searchBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                dataHandler.filterDataByTags(arrayListOf(newText))
                recyclerView.swapAdapter(MyAdapter(dataHandler.filteredData), true)
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                dataHandler.filterDataByTags(arrayListOf(query))
                recyclerView.swapAdapter(MyAdapter(dataHandler.filteredData), true)
                return true
            }

        })
        searchBar.setOnCloseListener { searchBarClosed() }

        if (!isExternalStorageWritable())
        {
            Toast.makeText(this, "No writable ext storage!", Toast.LENGTH_SHORT).show()
        }
        else
            if (!isExternalStorageReadable())
            {
                Toast.makeText(this, "No readable ext storage!", Toast.LENGTH_SHORT).show()
            }
            else
            {
                Log.e(LOG_TAG, "Ext storage available")

                dirPath = getExternalFilesDir(null)
                configFile = File(dirPath, "config.dat")

                if (!configFile.exists())
                {
                    Log.e(LOG_TAG, "File does not exist")
                    configFile.createNewFile()
                }
                else
                {
                    if (configFile.length() != 0L)
                    {
                        dataHandler.myData = Klaxon().parseArray<Item>(configFile.readText()) as ArrayList<Item>
                        Log.e(LOG_TAG, configFile.readText())
                    }
                    dataHandler.filteredData = dataHandler.myData

                    dataHandler.updateTags()
                }
            }

        viewManager = LinearLayoutManager(this)
        viewAdapter = MyAdapter(dataHandler.filteredData)

        recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }

        oldFilterItem = nav_view.menu.findItem(R.id.nav_clear_filter)
        nav_view.setNavigationItemSelectedListener { menuItem -> onNavItemSelected(menuItem)}

        when {
            intent?.action == Intent.ACTION_SEND -> {
                if ("text/plain" == intent.type) {
                    handleSendText(intent) // Handle text being sent
                }
            }
        }

        fab.setOnClickListener { view -> floatingButtonPressed(view)}
    }

    private fun onNavItemSelected(menuItem: MenuItem): Boolean
    {
        var filter: String = ""
        if (
            menuItem.title.toString().compareTo(getString(R.string.clear_filter)) != 0 &&
            menuItem != oldFilterItem)
        {
            filter = menuItem.title.toString().substringBeforeLast(" ")
            oldFilterItem = menuItem
        }
        else
        if (menuItem == oldFilterItem)
        {
            val noFilterItem = nav_view.menu.findItem(R.id.nav_clear_filter)
            noFilterItem.isChecked = true
            oldFilterItem = noFilterItem
        }

        dataHandler.filterDataByTag(filter)
        recyclerView.swapAdapter(MyAdapter(dataHandler.filteredData), true)
        drawer_layout.closeDrawers()
        return true
    }
    private fun searchBarClosed(): Boolean {
        dataHandler.filterDataByTags(arrayListOf())
        recyclerView.swapAdapter(MyAdapter(dataHandler.filteredData), true)
        return true
    }

    private fun toggleSearchBar() {
        if (searchBar.visibility == View.VISIBLE)
        {
            searchBar.visibility = View.GONE
        }
        else
        {
            searchBar.visibility = View.VISIBLE
        }
    }

    private fun handleSendText(intent: Intent) {
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            // Update UI to reflect text being shared
            val tag = packageManager.getApplicationLabel(packageManager.getApplicationInfo(referrer.host, PackageManager.GET_META_DATA)).toString()
            dataHandler.addData(Item(it, tag = tag))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun floatingButtonPressed(view: View)
    {
        mCounter++

        dataHandler.addData(Item("Click #" + mCounter))

        Toast.makeText(this, "Hello Kotlin, #" + mCounter + "!", Toast.LENGTH_SHORT).show()
    }

    /* Checks if external storage is available for read and write */
    fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    /* Checks if external storage is available to at least read */
    fun isExternalStorageReadable(): Boolean {
        return Environment.getExternalStorageState() in
                setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY)
    }
}

class MyAdapter(var dataSet: ArrayList<Item>) : RecyclerView.Adapter<MyAdapter.MyViewHolder>()
{
    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view)
    {
        var x: Float = 0F
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder_layout, parent, false) as View

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        val item: Item = dataSet[position]
        // - replace the contents of the view with that element
        val dateTime = LocalDateTime.parse(item.timeStamp)

        holder.view.txtCreationTime.text = dateTime.format(DateTimeFormatter.ISO_DATE) + "@" + dateTime.format(DateTimeFormatter.ISO_TIME)
        holder.view.txtContent.text = item.content
        holder.view.txtDiffTime.text = getTimeDiffString(dateTime)

        holder.view.imageButton.setOnClickListener { view -> checkBoxPressed(view, item)}
        holder.view.setOnTouchListener { v, event -> itemTouched(event, holder, item)}
        holder.view.imageButton.setBackgroundResource(0)
    }

    fun getTimeDiffString(dateTime: LocalDateTime): String
    {
        var diffTime = Duration.between(dateTime,LocalDateTime.now())
        var output = ""

        // compute number of days
        val numDays = diffTime.toDays()
        if (numDays > 0)
        {
            output += numDays.toString() + "d "
            diffTime = diffTime.minusDays(numDays)
        }

        // compute number of hours
        val numHours = diffTime.toHours()
        if (numHours > 0)
        {
            output += numHours.toString() + "h "
            diffTime = diffTime.minusHours(numHours)
        }

        // compute number of minutes
        val numMinutes = diffTime.toMinutes()
        if (numMinutes > 0)
        {
            output += numMinutes.toString() + "m "
            diffTime = diffTime.minusMinutes(numMinutes)
        }

        // compute number of seconds
        val numMillis = diffTime.toMillis()
        if (numMillis > 1000)
        {
            output += (numMillis / 1000).toString() + "s"
        }

        return output
    }

    override fun getItemCount() = dataSet.size

    fun checkBoxPressed(view: View, data: Item)
    {
        dataHandler.removeData(data)
    }

    fun itemTouched(
        e: MotionEvent,
        holder: MyViewHolder,
        item: Item): Boolean
    {
        when (e.action)
        {
            MotionEvent.ACTION_DOWN ->
            {
                holder.x = e.x
            }
            MotionEvent.ACTION_UP ->
            {
                val x =  e.x
                val delta = x - holder.x

                if (Math.abs(delta) > MIN_DISTANCE)
                {
                    if (delta > 0)
                    {
                        Log.e(LOG_TAG, "Left to right")
                        dataHandler.removeData(item)
                    }
                    else
                    {
                        Log.e(LOG_TAG, "Right to left")
                    }
                }
            }
        }

        return true
    }
}