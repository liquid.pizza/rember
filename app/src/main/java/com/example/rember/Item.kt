package com.example.rember

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

data class Item (
    val content: String,
    val counter: Int = 0,
    val timeStamp: String = LocalDateTime.now().toString(),
    val tag: String = "Rember")
{
}

data class ItemList (val items: ArrayList<Item>)
{
}